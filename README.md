# Тестовое задание #

Написать поисковый модуль, который находит элементы на главных
страницах сторонних сайтов (например, ссылки или картинки).
Модуль должен быть написан как полноценное php-приложение с
использованием концепции MVC.
  
На главной странице изначально выводится форма ввода адреса
сайта и выбора типа поиска (ссылки, изображения, текст). При выборе
«текст» появляется поле ввода, при выборе другого пункта исчезает.
Данные отправляются на сервер AJAX-запросом. Валидация всех
полей должна быть как на стороне клиента, так и на стороне сервера,
сообщения об ошибке валидации выводить без перезагрузки страницы.
Скрипт должен искать на указанной странице сайта (страница
скачивается с помощью CURL или другим способом) выбранные
элементы, используя регулярные выражения. Адрес сайта, найденные
элементы (одной строкой) и число найденных элементов записывать в БД
(PostgreSQL или MySQL).
  
Страница просмотра результатов – список проверенных сайтов и
числа найденных элементов. По клику на адрес сайта осуществляется
вывод найденных значений.
  
Шаблонизатор использовать любой, нельзя использовать PHP-
Фреймворки. Можно использовать jQuery, MooTools, Sencha или аналоги.

  

# Установка #
1. Импортировать в БД schema.sql
2. Скопировать config.php.default в config.php, настроить в config.php соединение с БД
3. Выполнить команду composer install