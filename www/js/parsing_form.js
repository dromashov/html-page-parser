(function() {

    $(document).ready(function() {

        $('#parsing-form input[name=parsingType]').change(function() {
            if ($('#parsing-form input[name=parsingType]:checked').val() == 'text') {
                $('#parsing-form input[name=text]').show();
            } else {
                $('#parsing-form input[name=text]').hide();
            }
        });

        $('#parsing-form').submit(function() {
            var $form = $(this);
            var $submitBtn = $(this).find('[type=submit]');
            clearNotifiers();
            if (isValidForm()) {
                $submitBtn.button('loading');
                $.post('/parser/parse', $form.serialize())
                    .done(function(response) {
                        if (response.status == 'ok') {
                            $.notify('Обработано и сохранено', 'success');
                        } else {
                            $.notify(response.errorMessage, 'error');
                        }
                    })
                    .always(function() {
                        $submitBtn.button('reset');
                    });
            }

            return false;
        });
    });

    function isValidForm() {
        var $form = $('#parsing-form');

        var $url = $form.find('input[name=url]');
        var $text = $form.find('input[name=text]');
        var $parsingType = $form.find('input[name=parsingType]:checked');

        if (!$url.val()) {
            $url.notify('Не задан URL', {position: 'right'});
            return false;
        }

        if (!isValidUrl($url.val())) {
            $url.notify('Неверный формат URL', {position: 'right'});
            return false;
        }

        if ($parsingType.val() == 'text') {
            if (!$text.val()) {
                $text.notify('Не задан Текст', {position: 'right'});
                return false;
            }
        }

        return true;
    }

})();


