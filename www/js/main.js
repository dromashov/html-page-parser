$.ajaxSetup({
    cache: false,
    timeout: 10000,
    dataType: 'json',
    error: function() {
        $.notify('Произошла ошибка', 'error');
    }
});

$.notify.defaults({
    globalPosition: 'bottom left'
});

function isValidUrl(url) {
    var regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(url);
}

function clearNotifiers() {
    $('.notifyjs-container, .notifyjs-arrow').hide();
}