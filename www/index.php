<?php

use HtmlPageParser\Core\App;
use HtmlPageParser\Core\Router;
use HtmlPageParser\Core\HttpException;

// Подключение конфига
$configFilePath = '../config.php';
if (!file_exists($configFilePath)) {
    throw new Exception('Config file does not exist.');
}
$config = require($configFilePath);

// Подключение файла autoload.php, сгенерированный композером
$composerAutoloadFilePath = '../vendor/autoload.php';
if (!file_exists($composerAutoloadFilePath)) {
    throw new Exception('Autoloader file does not exist.');
}
require($composerAutoloadFilePath);

$errorAction = [
    'controller' => 'parser',
    'action' => 'error',
];

Router::$defaultController = 'parser';
// Запуск приложения
try {
    App::run($config);
} catch (HttpException $e) {
    $errorAction['params'] = [
        'httpCode' => $e->getCode(),
        'errorMessage' => $e->getMessage()
    ];
    Router::runAction($errorAction);
} catch (Exception $e) {
    $errorAction['params'] = [
        'httpCode' => 500,
        'errorMessage' => $e->getMessage(),
        'errorCode' => $e->getCode(),
    ];
    Router::runAction($errorAction);
}