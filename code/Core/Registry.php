<?php

namespace HtmlPageParser\Core;

/**
 * Class Registry
 * Реестр.
 *
 * @package HtmlPageParser\Core
 */
class Registry
{
    /**
     * @var array
     */
    private static $registry = [];

    /**
     * @param string $key
     * @param mixed $value
     */
    public static function set($key, $value)
    {
        self::$registry[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        return isset(self::$registry[$key]) ? self::$registry[$key] : null;
    }
} 