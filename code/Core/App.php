<?php

namespace HtmlPageParser\Core;

/**
 * Class App
 * Класс приложения.
 *
 * @package HtmlPageParser\Core
 */
class App
{
    /**
     * Инициализирует и запускает приложение.
     *
     * @param array $config
     */
    public static function run($config = [])
    {
        Registry::set('config', $config);
        Registry::set('pdo', self::createPdo());

        Router::run(Request::getUri());
    }

    /**
     * Создает объект PDO.
     *
     * @return \PDO
     */
    protected static function createPdo()
    {
        $dbConfig = Registry::get('config')['db'];
        $pdo = new \PDO(
            $dbConfig['dsn'],
            $dbConfig['username'],
            $dbConfig['password']
        );
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }
} 