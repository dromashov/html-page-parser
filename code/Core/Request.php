<?php

namespace HtmlPageParser\Core;

/**
 * Class Request
 * Класс для работы с запросом.
 *
 * @package HtmlPageParser\Core
 */
class Request
{
    /**
     * Возвращает текущий URI
     *
     * @return string
     */
    public static function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Возвращает значение GET-параметра.
     *
     * @param string $key
     * @param bool $required
     * @throws HttpException
     * @return string|null
     */
    public static function getParam($key, $required = false)
    {
        $value = isset($_GET[$key]) ? $_GET[$key] : '';
        if ($required && $value === '') {
            throw new HttpException('Bad Request.', 400);
        }

        return $value;
    }

    /**
     * Возвращает значение POST-параметра.
     *
     * @param string $key
     * @param bool $required
     * @throws HttpException
     * @return string|null
     */
    public static function getPostParam($key, $required = false)
    {
        $value = isset($_POST[$key]) ? $_POST[$key] : '';
        if ($required && $value === '') {
            throw new HttpException('Bad Request.', 400);
        }

        return $value;
    }

    /**
     * Проверяет, является ли запрос ajax.
     *
     * @return bool
     */
    public static function isAjax()
    {
        return
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
} 