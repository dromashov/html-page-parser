<?php

namespace HtmlPageParser\Core;

/**
 * Class View
 * Класс для генераций шаблонов.
 *
 * @package HtmlPageParser\Core
 */
class View
{
    /**
     * @var string Название шаблона-лейаута
     */
    protected $layoutTemplate = 'layout';
    /**
     * @var string Путь к шаблонам
     */
    protected $templatesPath;

    /**
     * @var string Название страницы
     */
    protected $pageTitle;

    /**
     * Генерирует и возвращает содержимое по заданному шаблону
     *
     * @param string $templateName
     * @param array $data
     * @return string
     * @throws \Exception
     */
    public function renderTemplate($templateName, array $data = [])
    {
        $filePath = $this->getTemplatesPath() . DIRECTORY_SEPARATOR . $templateName . '.php';

        return $this->renderFile($filePath, $data);
    }

    /**
     * Генерирует и возвращает содержимое в лейауте по заданному шаблону
     *
     * @param string $templateName
     * @param array $data
     * @return string
     */
    public function renderPage($templateName, array $data = [])
    {
        $content = $this->renderTemplate($templateName, $data);

        return $this->renderTemplate($this->layoutTemplate, compact('content', 'templateName'));
    }

    /**
     * @param string $layoutTemplate
     */
    public function setLayoutTemplate($layoutTemplate)
    {
        $this->layoutTemplate = $layoutTemplate;
    }

    /**
     * Возвращает путь к шаблонам
     *
     * @return string
     */
    public function getTemplatesPath()
    {
        return isset($this->templatesPath)
            ? $this->templatesPath
            : __DIR__ . DIRECTORY_SEPARATOR .'..' . DIRECTORY_SEPARATOR . 'Views';
    }

    /**
     * @param string $templatesPath
     */
    public function setTemplatesPath($templatesPath)
    {
        $this->templatesPath = $templatesPath;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    /**
     * Генерирует и возвращает содержимое по шаблону файла.
     *
     * @param string $filePath
     * @param array $data
     * @return string
     * @throws \Exception
     */
    private function renderFile($filePath, array $data = [])
    {
        extract($data);
        ob_start();
        if (!file_exists($filePath)) {
            throw new \Exception('"' . $filePath . '" does not exist');
        }
        require($filePath);
        $content = ob_get_contents();
        ob_end_clean();

        return  $content;
    }
} 