<?php

namespace HtmlPageParser\Core;

/**
 * Class HttpException
 * Класс исключений, коды ошибок которых совпадают с кодами http-ответов.
 *
 * @package HtmlPageParser\Core
 */
class HttpException extends \Exception
{

} 