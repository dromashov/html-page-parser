<?php

namespace HtmlPageParser\Core;

/**
 * Class DbModel
 * Общий класс для моделей, работающих с БД.
 *
 * @package HtmlPageParser\Core
 */
class DbModel extends Model
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * @param \PDO $pdo
     */
    public function setPdo(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function __construct()
    {
        $this->pdo = Registry::get('pdo');
    }
} 