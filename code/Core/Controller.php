<?php

namespace HtmlPageParser\Core;

/**
 * Class Controller
 * Базовый класс для контроллеров.
 *
 * @package HtmlPageParser\Core
 */
class Controller
{
    /**
     * @var View
     */
    protected $view;

    public function __construct()
    {
        $this->view = new View();
    }
} 