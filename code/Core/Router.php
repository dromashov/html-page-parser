<?php

namespace HtmlPageParser\Core;

/**
 * Class Router
 * Класс для маршрутизации запросов.
 *
 * @package HtmlPageParser\Core
 */
class Router
{
    public static $defaultController = 'main';
    public static $defaultAction = 'index';

    /**
     * Парсит URI и запускает экшен контроллера.
     *
     * @param string $uri
     */
    public static function run($uri)
    {
        $action = self::parseUri($uri);
        self::runAction($action);
    }

    /**
     * Запускает экшен контроллера с заданными параметрами.
     *
     * @param array $action в формате, возвращаемый методом self::parseUri
     * @throws HttpException
     */
    public static function runAction(array $action)
    {
        $controllerName = isset($action['controller']) ? $action['controller'] : self::$defaultController;
        $actionName     = isset($action['action']) ? $action['action'] : self::$defaultAction;
        $params         = isset($action['params']) ? $action['params'] : [];

        $controllerClassName = '\\HtmlPageParser\\Controllers\\' . ucfirst($controllerName) . 'Controller';
        if (class_exists($controllerClassName)) {
            $controllerObject = new $controllerClassName();
            $actionMethodName = $actionName . 'Action';
            if (
                $controllerObject instanceof Controller &&
                method_exists($controllerObject, $actionMethodName)
            ) {
                echo call_user_func_array([$controllerObject, $actionMethodName], $params);
                return;
            }
        }

        throw new HttpException('Page not found.', 404);
    }

    /**
     * Парсит URI и возвращает массив с ключами controller, action и params
     *
     * @return array
     */
    private static function parseUri($uri)
    {
        $segments = explode('/', $uri);

        return [
            'controller' => !empty($segments[1]) ? $segments[1] : null,
            'action'     => !empty($segments[2]) ? $segments[2] : null,
            'params'     => array_filter(array_slice($segments, 3))
        ];
    }
} 