<?php

namespace HtmlPageParser\Core;

/**
 * Class Model
 * Базовый класс для моделей.
 *
 * @package HtmlPageParser\Core
 */
class Model
{
    /**
     * @var int
     */
    private $errorCode;
    /**
     * @var string
     */
    private $errorMessage;

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param int $errorCode
     * @param string $errorMessage
     */
    public function setError($errorCode, $errorMessage)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return isset($this->errorCode);
    }

    /**
     * Устанавливает аттрибуты модели по заданному массиву.
     *
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    /**
     * Возвращает объект модели.
     *
     * @return static
     */
    public static function model()
    {
        $model = new static();

        return $model;
    }
}