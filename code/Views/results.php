<?php $this->setPageTitle('Результаты') ?>

<h5>Обработанные страницы:</h5>

<?php foreach ($pages as $page) : ?>
    <?= $page->getId() ?>) <a href="/parser/results/<?= $page->getId() ?>"><?= $page->getUrl() ?></a>
    <i>(<?= $page->getElementsCount() ?>)</i>
    <br>
<?php endforeach ?>