<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $this->getPageTitle() ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/css/main.css" rel="stylesheet" media="screen">
</head>
<body>

<div id="main-wr">

    <div id="header">

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Html Page Parser</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?= $templateName == 'main' ? 'class="active"' : '' ?>>
                            <a href="/">Парсер</a>
                        </li>
                        <li <?= $templateName == 'results' || $templateName == 'result' ? 'class="active"' : '' ?>>
                            <a href="/parser/results">Результаты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    </div>

    <div id="content">
        <?php echo $content; ?>
    </div>

    <div id="footer">

    </div>
</div>

<script
    src="//code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/parsing_form.js"></script>

</body>
</html>