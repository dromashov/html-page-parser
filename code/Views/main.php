<?php $this->setPageTitle('Главная') ?>

<h5>Парсить:</h5>

<form id="parsing-form">

    <div class="form-group">
        <input type="text" class="form-control" placeholder="URL" name="url">
    </div>

    <div class="form-group">
        <input type="text" class="form-control" placeholder="Текст" name="text" style="display: none">
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="parsingType" value="images" checked>
            Изображения
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="parsingType" value="links">
            Ссылки
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="parsingType" value="text">
            Текст
        </label>
    </div>
    <button type="submit" class="btn btn-default" data-loading-text="Загрузка...">Парсить</button>
</form>