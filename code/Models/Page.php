<?php

namespace HtmlPageParser\Models;

use HtmlPageParser\Core\DbModel;

/**
 * Class Page
 * Класс для работы с таблицей pages.
 *
 * @package HtmlPageParser\Models
 */
class Page extends DbModel
{
    const ELEMENTS_SEPARATOR = '~~';

    /** @var string  */
    private $tableName = 'pages';

    /** @var  int */
    protected $id;
    /** @var  string */
    protected $url;
    /** @var  array */
    protected $elements;
    /** @var  int */
    protected $elements_count;

    /**
     * Создает модель на основе массива.
     *
     * @param array $page
     * @return Page
     */
    public static function createModel(array $page)
    {
        $model = new self();
        $model->url = isset($page['url']) ? $page['url'] : '';
        $model->elements = isset($page['elements']) ? $page['elements'] : '';
        $model->elements_count = isset($page['elements']) ? count($page['elements']) : 0;

        return $model;
    }

    /**
     * Вставка модели в таблицу.
     */
    public function insert()
    {
        $stmt = $this->pdo->prepare(
            'INSERT INTO ' . $this->tableName .
            ' (url, elements, elements_count)' .
            ' VALUES (:url, :elements, :elements_count)'
        );
        $stmt->bindValue('url', $this->url);
        $stmt->bindValue('elements', $this->serializeElements($this->elements));
        $stmt->bindValue('elements_count', $this->elements_count);
        $stmt->execute();
    }

    /**
     * Ищет модель по id.
     *
     * @param $id
     * @return Page
     */
    public static function findByPk($id)
    {
        $stmt = self::model()->pdo->prepare(
            'SELECT * FROM ' . self::model()->tableName .
            ' WHERE id = :id'
        );
        $stmt->bindValue('id', $id);
        $stmt->execute();

        $result =  $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }
        $model = self::fetchedArrayToModel($result);

        return $model;
    }

    /**
     * Возвращает все модели из таблицы.
     *
     * @return array
     */
    public static function findAll()
    {
        $stmt = self::model()->pdo->prepare(
            'SELECT * FROM ' . self::model()->tableName .
            ' LIMIT 1000'
        );
        $stmt->execute();

        $result =  $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $models = [];

        foreach ($result as $row) {
            $models[] = self::fetchedArrayToModel($row);
        }

        return $models;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param array $elements
     */
    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    /**
     * @return int
     */
    public function getElementsCount()
    {
        return $this->elements_count;
    }

    /**
     * @param int $elementsCount
     */
    public function setElementsCount($elementsCount)
    {
        $this->elements_count = $elementsCount;
    }

    /**
     * Преобразует элементы из массива в строку.
     *
     * @param array $elements
     * @return string
     */
    private function serializeElements(array $elements)
    {
        return implode(self::ELEMENTS_SEPARATOR, $elements);
    }

    /**
     * Преобразует элементы из строки в массив.
     *
     * @param string $elements
     * @return array
     */
    private function unSerializeElements($elements)
    {
        return explode(self::ELEMENTS_SEPARATOR, $elements);
    }

    /**
     * Создает модель на основе массива, извлеченного из БД.
     *
     * @param array $page
     * @return Page
     */
    private static function fetchedArrayToModel(array $page)
    {
        $model = new self();
        $model->setAttributes($page);
        $model->elements = $model->unSerializeElements($model->elements);

        return $model;
    }
}