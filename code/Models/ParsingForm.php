<?php

namespace HtmlPageParser\Models;

use HtmlPageParser\Classes\Browsers\CurlBrowser;
use HtmlPageParser\Classes\Parsers\Parser;
use HtmlPageParser\Core\Model;

/**
 * Class ParsingForm
 * Класс для работы с формой парсинга страниц.
 *
 * @package HtmlPageParser\Models
 */
class ParsingForm extends Model
{
    const ERROR_EMPTY_URL = 1;
    const ERROR_INVALID_URL = 2;
    const ERROR_EMPTY_PARSING_TYPE = 3;
    const ERROR_EMPTY_TEXT = 4;

    /** @var  string */
    protected $url;
    /** @var  string */
    protected $parsingType;
    /** @var  string */
    protected $text;

    /**
     * Валидирует форму.
     *
     * @return bool
     */
    public function validate()
    {
        if (!$this->url) {
            $this->setError(self::ERROR_EMPTY_URL, 'Не задан URL');
            return false;
        }

        if (!filter_var($this->url, FILTER_VALIDATE_URL)) {
            $this->setError(self::ERROR_INVALID_URL, 'Неверный формат URL');
            return false;
        }

        if (!$this->parsingType) {
            $this->setError(self::ERROR_EMPTY_PARSING_TYPE, 'Не задан тип парсинга');
            return false;
        }

        if ($this->parsingType == 'text' && !$this->text) {
            $this->setError(self::ERROR_EMPTY_TEXT, 'Не задан Текст');
            return false;
        }

        return true;
    }

    /**
     * Обрабатывает форму.
     *
     * @return bool
     * @throws \Exception
     */
    public function handle()
    {
        if (!$this->validate()) {
            return false;
        }

        $browser = new CurlBrowser();
        $parser = Parser::create($this->parsingType);

        if ($this->parsingType == 'text') {
            $parser->setText($this->text);
        }

        $parsingJob = new ParsingJob($this->url, $browser, $parser);
        $parsingJob->run();

        return true;
    }
} 