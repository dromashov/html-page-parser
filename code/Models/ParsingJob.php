<?php

namespace HtmlPageParser\Models;

use HtmlPageParser\Classes\Browsers\Browser;
use HtmlPageParser\Classes\Parsers\Parser;
use HtmlPageParser\Core\Model;

/**
 * Class ParsingJob
 * Класс, содержащий логику чтения, парсинга и сохранения результатов.
 *
 * @package HtmlPageParser\Models
 */
class ParsingJob extends Model
{
    /** @var  string */
    private $url;
    /** @var  Browser */
    private $browser;
    /** @var  Parser */
    private $parser;

    /**
     * @param string $url
     * @param Browser $browser
     * @param Parser $parser
     */
    public function __construct($url, Browser $browser, Parser $parser)
    {
        $this->url = $url;
        $this->browser = $browser;
        $this->parser = $parser;
    }

    /**
     * Парсит страницу с заданным $url и сохраняет результат.
     */
    public function run()
    {
        $content = $this->browser->read($this->url);
        $matches = $this->parser->parse($content);

        $page = Page::createModel([
            'url' => $this->url,
            'elements' => $matches
        ]);

        $page->insert();
    }
} 