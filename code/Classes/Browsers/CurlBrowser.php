<?php

namespace HtmlPageParser\Classes\Browsers;

/**
 * Class CurlBrowser
 * Класс для получения содержимого веб-страницы через curl.
 *
 * @package HtmlPageParser\Classes\Browsers
 */
class CurlBrowser extends Browser
{
    /**
     * @param string $url
     * @return string
     * @inheritdoc
     */
    public function read($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \Exception('Invalid URI');
        }

        $curl     = curl_init($url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content  = curl_exec($curl);
        $errorCode     = curl_errno($curl);
        $errorMessage  = curl_error($curl);
        curl_close($curl);

        if ($errorCode) {
            throw new \Exception($errorMessage, $errorCode);
        }

        return $content;
    }
}