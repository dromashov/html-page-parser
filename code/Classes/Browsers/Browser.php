<?php

namespace HtmlPageParser\Classes\Browsers;

/**
 * Class Browser
 * Базовый класс для браузеров.
 *
 * @package HtmlPageParser\Classes\Browsers
 */
abstract class Browser
{
    /**
     * Получает содержимое страницы с заданным URL.
     *
     * @param string $url
     * @return mixed
     */
    abstract public function read($url);
} 