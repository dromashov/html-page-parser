<?php

namespace HtmlPageParser\Classes\Parsers;

/**
 * Class ImagesParser
 * Парсер изображений.
 *
 * @package HtmlPageParser\Classes\Parsers
 */
class ImagesParser extends Parser
{
    /**
     * Парсит изображения.
     *
     * @param string $content
     * @return array
     */
    public function parse($content)
    {
        preg_match_all('/<img.*?src=(\"|\')(.*?)(\"|\')/i', $content, $matches);

        return $matches[2];
    }
}