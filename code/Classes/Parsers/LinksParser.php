<?php

namespace HtmlPageParser\Classes\Parsers;

/**
 * Class LinksParser
 * Парсер ссылок.
 *
 * @package HtmlPageParser\Classes\Parsers
 */
class LinksParser extends Parser
{
    /**
     * Парсит ссылки.
     *
     * @param string $content
     * @return array
     */
    public function parse($content)
    {
        preg_match_all('/<a.*?href=(\"|\')(.*?)(\"|\')/i', $content, $matches);

        return $matches[2];
    }
}