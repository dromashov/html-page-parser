<?php

namespace HtmlPageParser\Classes\Parsers;

/**
 * Class Parser
 * Базовый класс для парсеров.
 *
 * @package HtmlPageParser\Classes\Parsers
 */
abstract class Parser
{
    /**
     * Парсит содержимое.
     *
     * @param $content
     * @return array
     */
    abstract public function parse($content);

    /**
     * Метод-фабрика для создания парсера с заданным типом.
     *
     * @param string $parserType
     * @throws \Exception
     * @return Parser
     */
    public static function create($parserType)
    {
        $parserClass = __NAMESPACE__ . '\\' . ucfirst($parserType) . 'Parser';
        if ($parserType && class_exists($parserClass)) {
            return new $parserClass();
        }

        throw new \Exception('Incorrect parser type.');
    }
} 