<?php

namespace HtmlPageParser\Classes\Parsers;

/**
 * Class TextParser
 * Парсер текста.
 *
 * @package HtmlPageParser\Classes\Parsers
 */
class TextParser extends Parser
{
    /** @var string  */
    private $text = '';

    /**
     * Парсит текст.
     *
     * @param string $content
     * @return array
     */
    public function parse($content)
    {
        if (empty($this->text)) {
            return [];
        }

        preg_match_all('/' . strip_tags($this->text) . '/i', $content, $matches);

        return $matches[0];
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}