<?php

namespace HtmlPageParser\Controllers;

use HtmlPageParser\Core\Controller;
use HtmlPageParser\Core\Registry;
use HtmlPageParser\Core\Request;
use HtmlPageParser\Models\Page;
use HtmlPageParser\Models\ParsingForm;

class ParserController extends Controller
{
    public function errorAction($httpCode, $errorMessage = '', $errorCode = 0)
    {
        http_response_code($httpCode);

        // По хорошему здесь пишем $errorMessage и $errorCode в лог.

        if ($httpCode == 404) {
            $errorMessage = 'Страница не найдена';
        } elseif (!Registry::get('config')['display_sys_errors']) {
            $errorMessage = 'Внутренняя ошибка';
        }

        if (Request::isAjax()) {
            return $errorMessage;
        } else {
            return $this->view->renderPage('error', compact('errorMessage'));
        }

    }

    public function indexAction()
    {
        return $this->view->renderPage('main');
    }

    public function resultsAction($id = '')
    {
        if ($id) {
            $page = Page::findByPk($id);
            return $this->view->renderPage('result', compact('page'));
        }
        else {
            $pages = Page::findAll();
            return $this->view->renderPage('results', compact('pages'));
        }
    }

    public function parseAction()
    {
        $parsingForm = new ParsingForm();
        $parsingForm->setAttributes($_POST);
        $parsingForm->handle();

        if ($parsingForm->hasError()) {
            return json_encode([
                'status' => 'error',
                'errorMessage' => $parsingForm->getErrorMessage(),
            ]);
        }

        return json_encode([
            'status' => 'ok',
        ]);
    }
} 